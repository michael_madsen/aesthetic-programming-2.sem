# Minix: DEsigning a "Throbber"

![ALT](Screenshot_2021-02-21_at_18.11.29.png)![ALT](05onfire1_xp-superJumbo-v2.jpg)

[My Minix3](https://michael_madsen.gitlab.io/aesthetic-programming-2.sem/Minix3/index.html)

[My Code](https://michael_madsen.gitlab.io/aesthetic-programming-2.sem/Minix3/sketch.js)


## What do you want to explore and/or express?
In this minix I have tried to convey the frustrating and distracting nature of the throbber. My design combines "This is fine" meme-dog with harry potter. The dog symbolises the passiv-agressive frustration users feel when trying to find patience while experiencing a looooong throbber/loading element. The harry potter train is exitement and adventure ruined by an endless loop, the journey never ends, destination never reached.

In this minix I have praticed:

1. For loops + figures
2. variable manipulation in conditional structure

###  For loops and figures
I have practiced using `for loop` to make exact copies of a sketch in `function draw (){`. In my throbber i have created a `for loop` which increments 190 to `i`. `i`is tested if it is`<` 400. This is how my loop works:

for(let i = 0;i < 400; i=i +190 ){    
// code/sketch that will be copied 
equal to the amount of iterations/ loops
}
1. is 0 less than 400? yes= 1 loop/1 sketch =add 190 to `i`
 2. is 190 less than 400? yes=1 loop/i sketch =add 190 to `i`
 3. is 380 less than 400? yes=1 loop/i sketch =add 190 to `i`
 4. is 570 less than 400? No= 3x iteration/loops will be exicuted 

The shapes which the dog and train are constructed of are placed in souch a way that they utilize the way that the `for loo` lines everything up. This lining up of shapes means that i essentially can "hide" unwanted shapes beneath another, since the `fo`loop`creates identical copies.
### variable manipulation in conditional structure

In all my figures i have manually added `x` to `i `, there must be an easier way to do this, but anyways, this linking to x means that my conditional structure see here:

if (x > 400 || x < 0) {// creates the motion-loop, by resetting x by multiplying with -1
              speed = speed * -1;
          }
          x = x + speed


Will add a negative value to `x`through `speed`if `x` is less than 400(this controls how far the train goes) `OR` `x` is less than 0.
## What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way? How is time being constructed in computation (refer to both the reading materials and your coding)? 

The `function draw()` itself is a loop. Combined with my for loop it creates 3x copies of the same construct, we see them on the screen because they are drawn over and over again because the `for loop`ìs in the `function draw()`. Computation here "freezes" a momment that constantly redraws x amount of shapes and text.

My  `variabe speed` creates the illusion of movement/progression by resetting the x value. Since the `x value` is link to the `i`which controls the location of the figures on the x-axis, the figures will move acordingly.


"_This “problem of decision,” or “ending” as Ernst puts it, underscores broader notions of algorithmic time and the way the computer forever anticipates its own “never-ending” in an endless loop. Perhaps the throbber icon is a good metaphor for this__"(Cox & Soon 2020:94).

This qoute from our main textbook states that the computer is neverending and the throbber a metaphor. The metaphor is understood by us because the repetition cycle of the throbber translates the computers "no-time" to something that symbolizes stunted progressen and/or regression. An example is the swirling form of the traditional throbber and the backtraccking train that is my design.

## Think about a throbber that you have encounted in digital culture, e.g. for streaming video on YouTube or loading the latest feeds on Facebook, or waiting for a payment transaction, and consider what a throbber communicates, and/or hides? How might we characterize this icon differently?

I have thought about throbbers and that is why I made the train and the dog. The circular motion of the traditional throbber may mirror the computer´s "never-ending", but thats not very reassuring for me. I like progression bars or something entertaining, but it´s ofcourse important to rember that what we see are only representations of whats going on. 
