
/*Global variables that sets
the start value for x, which will be utilised by "speed"
and makes the transition speed 2*/
let x = 0;
let speed = 2;



function setup() {

  createCanvas(windowWidth, windowHeight);

}
  //frameRate(8);
  //x=300;
  //speed=3



function draw(){
  background(random("255"));
  translate(width/3,height/3);



  if ( x <= 200) {// Aslong as this is true
  push();
  textFont('Cinzel');
  fill("red");
  textSize(40);
  text("This is fine",-40,-60);//Then its fine
  pop();
}




  for(let i = 0;i < 400; i=i +190 ){// the begiing of the for-loop

    push();
    fill("black");
    ellipse(x+i-20,110,120,140);
    pop();

    fill("#E0A02C");
    ellipse(x+i,50,120,140);


    push();// hat top
    fill("black");
      triangle(x+i-80, -50, x+i-50, 5, x+i+10, -20);
      pop();// hat end

    push();// eyes
    fill("white");
      ellipse(x+i+40,20,45,45);
      ellipse(x+i-15,40,45,55);
    pop();
    push();
    noStroke();
ellipse(x+i+20,90,100,40);
pop();
    push();// pupils
    fill("black");
      ellipse(x+i+50,20,25,35);
      ellipse(x+i-15,40,30,35);
    pop();


    push();//mouth
    fill("black");
        ellipse(x+i+35,80,60,50);
      pop();// mouth end

      push();//teeth
      fill("white");
      ellipse(x+i+45,92,14,10);
          ellipse(x+i+35,90,15,10);
          ellipse(x+i+25,85,20,15);
        pop();// teeth end

    push();//snout
    noStroke();
        ellipse(x+i+60,65,100,60);
      pop();//snout end

      push();//cheek
      noStroke();
          ellipse(x+i+5,75,50,20);
        pop();//cheek end

      push();//bop nose
      fill("black");
          ellipse(x+i+90,40,50,30);
        pop();// bop nose end

        push();// ear top
        fill("black");
          triangle(x+i-80, 75, x+i-50, 10, x+i-46, 65);
          pop();// ear end


          push();// ear tip
          fill("black");
            ellipse(x+i-63,72,35,39);
          pop();// ear tip end


          push();//wheels
          fill("black");
          ellipse(x+i-40,200,40,40);
          ellipse(x+i,200,40,40);
          ellipse(x+i+40,200,40,40);
          ellipse(x+i+80,200,40,40);
          pop();//wheels end

          push();//train
          fill("black");//front
          rect(x+i+140,120,20,50);
          fill("black");//front
          rect(x+i-80,140,260,60);
          fill("black");//roof
          rect(x+i-80,110,200,20);
          fill("red");//wagon
          rect(x+i-80,120,200,80);
          fill("gold");
          textFont('Benne');
          textSize(17);
          text(" F*cking Nowhere Express",x+i-80,160);
          pop();//train


          if (x > 400 || x < 0) {// creates the motion-loop, by resetting x by multiplying with -1
              speed = speed * -1;
          }
          x = x + speed;

  }//End of loop
}// End of draw function













/*
function windowResized(){
  resizeCanvas(windowWidth,windowHeight);
}

function draw() {
  background(70,80);
  drawElements();
}

function drawElements(){
  let num=6;
  push();
  translate(width/2,height/2);
  let cir=360/num*(frameCount%num);
  rotate(radians(cir));
  noStroke();
  fill(255,255,0);
  ellipse(35,0,22,22);
  pop();
  stroke(255,255,0);
  line(60,0,60,height);
}

function windowResized(){
  resizeCanvas(windowWidth,windowHeight);
}
*/
