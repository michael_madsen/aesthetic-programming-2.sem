function setup() {

  createCanvas(windowWidth, windowHeight, WEBGL);
//perfect-fit canvas

}







function draw() {
  background(220);
  rotateY(frameCount * 0.01);// the rate of rotation on the y-axis



  /*Following does not utilize "push" & "pop"
  head + torso*/
  fill("#AA8256");
  sphere(85);

  translate(20, 80);
  sphere(50);


  fill("#AA8256");
  translate(10, 50);
  sphere(45);

  translate(-30, 40);
  sphere(40);
  translate(-30, 10);
  sphere(35);

  translate(-40, 10);
  sphere(30);

  translate(-40, 0);
  sphere(25);

  translate(-35, -5);
  sphere(20);

  //stinger
  fill("#white");
  translate(-5, -40);
  rotate(120);
  cylinder(15, 30);

  translate(3, -29);
  rotate(60);
  cone(15, 30);

  //horn
  fill("#white");
  translate(60, 210);
  rotate(24);
  cone(50, 180);
  pop();


  //nose
  fill("#C381A3");
  translate(-60, -60);
  sphere(40);


  //eye left
  fill("#0E0F11");
  translate(10, 30, 50);
  sphere(10);

  //eye right
  fill("#0E0F11");
  translate(0, 0, -100);
  sphere(10);


  /*Following does not utilize "push" & "pop"
  arms/legs*/
  fill("#C381A3");
  translate(30, -120);
  sphere(15);


  translate(0, 5, 100);
  sphere(15);


  translate(2, -60, -10);
  sphere(15);


  translate(2, 5, -80);
  sphere(15);


  translate(60, -40, 80);
  sphere(15);

  translate(5, 0, -80);
  sphere(15);

/* Active use of "push" & "pop",
 4x sets of lower hands/feet closest to stinger */

 push();
 translate(40, 0);//lower right ball/hand/leg #1
 sphere(15);
 pop();

push();
translate(40, 0,80);//lower left ball/hand/leg #1
sphere(15);
pop();

push();
translate(75, 0,5);//lower right ball/hand/leg #2
sphere(15);
pop();

push();
translate(75, 0,75);//lower left ball/hand/leg #2
sphere(15);
pop();

push();
translate(120, 10,15);//lower right ball/hand/leg#3
sphere(15);
pop();

push();
translate(120, 10,60);//lower left ball/hand/leg#3
sphere(15);
pop();

push();
translate(150, 30,20);//lower right ball/hand/leg#4
sphere(15);
pop();

push();
translate(150, 30,60);//lower left ball/hand/leg#4
sphere(15);
pop();
}
