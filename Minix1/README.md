## Minix1: "Weedle"

![ALT](Minix1/weedle.jpg) 

For my first Minix I decided to create a Pokemon called Weedle, check my boi through this [link](https://michael_madsen.gitlab.io/aesthetic-programming-2.sem/Minix1/index.html)

In this exercise I have mainly worked with the syntaxes for "sphere", "cylinder","cone","translate","rotate" and "push"/"pop".

### Setting the scene, building the body

![ALT](Minix1/ssweedle.png)
### the perfects Canvas
I defined my canvas with windowHeight + windowLength, so the canvas would adjust itself for the screen. 

### The main body No push, No pop
The main body + head was made by playing around with the "sphere" shape. The horn and the stinger-tip are made with the "cone" + "cylinder" shape, I used "radion" values in "rotate" (not the most precise way of doing it, if you cant recall highschool math).

I spend many hours trying to align everything using "translate", it seemed easy, in theory:

- 1.parameter(x) would move the shapes along the x-axis/left+right
- 2.parameter(y) would move the shapes along the y-axis/up+down
- 3.parmeter (Z) would move the shapes along the z-axis/in.out

If you look at the function from line 22-105, the values are all over the place
![ALT](Minix1/ppp.png)

### Translate, accumulation and push-pop

So by reading the reference guide and watching a tutorial at "the coding train", I found out that the translate parameter accumulates, unless they are told otherwise. If you are not aware of how the "translate" stockpiles values, you wont be abble to predict or control the point of origin for your target shape.

This is where "push" and "pop" comes in handy. "push" initiates "translate" and "pop" ends it. I have actively used push and pop in the last parts of the function and it should be a bit more obvous how i tweak the parameters to position the shapes.

![ALT](Minix1/pp.png)

For coloring I used hex-codes instead of RGB values and i kept the decoration on the 3d shapes (because it looks cool), i can remove them by adding "noStroke();". For greater overview and control i can use "classes".

## How would you describe your first independent coding experience?

It was really fun. It is much more motivating to explore and combine syntaxes, when you are allowed to create whatever you want.

## How is the coding proces different from, or similar to, reading and writing text?

Coding is similar to writing when it comes to relations. In danish phonetics the vowel "A" will sound different dependent on the letters next to it, in the same way (code-wise) the parameters in "translate" will be diffrent dependent on if they are followed by another "translate" or isolated by "push"/"pop".

Coding is different from writing by its very specific syntax rules. If i write something, another human will be able read it if they know the language that i am writing in (english, danish etc.) even if i make a mistake they will be able to descern it. If however i dont use the correct syntax the computer wont be able to "read" it.

## What does code and programming mean to you, and how does the assined reading help you reflect on these terms?

Coding for me used to be something terrifying, cold and im-personal. Now i really enjoy it because I can have fun with it and not torture myself by always having to make something thats "useful". I believe that great value can be drawn from anything through which you can express yourself. 

The curiculum brings everything to a greater perspective. Learing why things are as they are is both exiting and also terrifying. 

#### ref.

reference photo: bulbapedia_bulbagarden.net/wiki/weedle_(pokemon)

The coding Train: 9.1: Transformations pt.1

P5.jsorg/Reference/translate
