# "Vocal load" an E-lit by Mark Nøregaard & Michael Madsen

![ALT](minix.gif) 

[Our project (while in chrome, click the screen to initiate the program)](https://mark_norgaard.gitlab.io/aesthetic-programming-mark/miniX8/?fbclid=IwAR1s2YXYA3VYjmE9A1GLlUM6tKlKv3vo6MFpslj8ICrGpuk7MVslLqKdIRk)

[Our source code)](https://gitlab.com/Mark_norgaard/aesthetic-programming-mark/-/blob/master/miniX8/sketch.js?fbclid=IwAR3alsIM_9ZVk82lLw_EtBF6hxpDPCuAJC2mcdn4LGGZeNGCxyFyLm4K12c)

The program prints out a snippet of its own syntax on the screen, which it then continues to read aloud. 

## Method
The idea we got inspired by was Michael's pitch about a self-reading program.
On friday we met up and discussed how this could be done. Mark found a tutorial for running through an array on w3schools, which was used as an initial template.

#### _The inspiration_
 ![ALT](Screenshot_2021-04-11_at_10.33.47.png) 


#### _The solution_
![ALT](ezgif.com-gif-maker.gif) 
![ALT](Hej_Michael.gif) 

Going from there we decided to work with the program separately. Michael ended up taking a different approach, since he had trouble tweaking the aforementioned code, creating a program that prints the text using values drawn from a JSON.file and using a curly-bracket “arrow” to steer the readers eyes. The program was then sent to Mark who promptly removed the curly bracket, and replaced it with a word for word appearing approach. Additionally there were recorded sound files for each word in the code, which was read and designed to sound like the program speaking to you.


## how your program works, and what syntax you have used, and learnt?

We learned what JSON was, how to create a file and how to refer to the wanted data via dot.notation. The loading and playing of many sound files without delay with the use of the preload function and also using the “startUserAudio” function on mouse press to bypass the autoplay policy of browsers such as chrome and safari.

The program works by first loading all the needed files (JSON and soundfiles in wav. format). Then it has a counter for how many frames that have passed since the start of the draw loop. This counter works as a delay for the appearing words accompanied by their spoken equivalent.

In the end it uses the noLoop(); function to stop counting the frames when the code has finished running.
Analyze your own e-lit work by using the text Vocable Code and/or The Aesthetics of Generative Code (or other texts that address code/voice/language).
Our work attempts to highlight the similarities between high level programming languages use of language in its syntaxes and how these words are used in daily practice. Noam Chomsky is referred to in Cox & Mclean’s text “vocable code”, for concluding that grammar is independent from meaning. Although we agree with this statement, we’ve noticed a connection between the understandability of code for the reader and the degree to which, the grammar of the code is equal to its meaning.
In our code we have attempted through the spoken word, to further connect the grammatical syntax of the code to its meaning. The written code on the screen connects the executed code to its written form, and the use of variable names making the sentences have more than just grammatical structure but also lyrical meaning, further connects the codes’ form and the form of day to day language. In addition to this, the chosen code is read aloud. This is meant to connect the words in the code to its pronunciation in the english language (the language in which the code is written), further connecting grammar and its meaning.

## How would you reflect on your work in terms of Vocable Code?
Vocabel code is the acknowledgment that programming languages share some properties with natural language, the structure of syntax being one of them and as an extension of this, its potential for expression.
“_If program code is like speech in as much as it does what it says, then it can also be said to be poetry inasmuch as it involves both written and spoken forms_” Cox & Mclean. P.17
Our code utilizes this potential by letting our program give a vocal performance of its own execution. The robotic voice is a playful mockery of the computer's own inability to perfectly recreate human speech, as addressed in “Embodiment” of Cox & Mclean. The program performs a speech act, which is the philosophical theory of expressions that holds both information and an action. The program says that it preloads and that the variable should be equal to data drawn from the named JSON.file and this is exactly what it performs. The text you see and the voice you hear are all data from the JSON file.

### references

- Aesthetic Programming: Soon & Cox
- Vocabel Code: Cox & Mclean
- 10.3: What is JSON? Part II - p5.js Tutorial: https://www.youtube.com/watch?v=118sDpLOClw&t=60s
- 10.2: What is JSON? Part I - p5.js Tutorial: https://www.youtube.com/watch?v=_NFkzw6oFtQ&t=778s
- https://www.w3schools.com/js/js_arrays.asp
