### This program does not work! Check my minix6 

![ALT](Screenshot_2021-03-08_at_08.06.25.png)

[My Minix4](https://michael_madsen.gitlab.io/aesthetic-programming-2.sem/Minix4/index.html)

[My Code](https://michael_madsen.gitlab.io/aesthetic-programming-2.sem/Minix4/sketch.js)


## Discription of piece

This "low floor" piece WAS supposed to be a playful, modest and primitive commontary on data capturing with ouputs based on limited data. The program should be able to take a string of text and output a list of words that rhymes with the last word of the input, in case of a sentence, or just the word. I wanted to show how judging an input on limited/dimensioned data, will give you limited/dimensioned results.

## Describe your program and what you have used and learnt.
The program is inspired by [thie piece](https://thisisamagazine.com/ELO/) from SLSAeu2021. I looked at their index.html and found the libriaries that they used.
My failure of a program is a text-field and a minimal styled button. Assigned to the field and the buttons is a function that utilizes( or should have) data from the RiTa.lexicon and RiTa´s Rhyme function. 

I have not learned so much, since the program did not work, I believe it is because the libriary file i downloaed from the source did not include the lexicon and without the lexicon the rhyme function wont be able to find words.

## Articulate how your program and thinking address the theme of “capture all.”
RiTa "captures all" by being able to take any string of text and convert it to a rita-string and from their you can draw data souch as nouns,verbs syllables. 

## What are the cultural implications of data capture?
One of the cultural implications of data capture, is that users through custimizations, helps systems classify them, ironically turning something that is thought to be unique to something that is boxable, class-able and potentially marketable.

# Source codes and references
https://editor.p5js.org/xinxin/sketches/CO19ciYn
https://www.youtube.com/watch?v=lIPEvh8HbGQ
