/*This sketch is taken from this video: https://www.youtube.com/watch?v=DEHsr4XicN8
With some small aditions added by me*/


/*The variable that
stores the new objects
in an array*/
let muslims=[];

function setup(){
  createCanvas(600,400);
  /*The number of objects to
  be born from the constructor.
  [i] will be less than 4 for
  4 iterations [0,1,2,3 stop]
  ,so we get 4 "Muslim" objects */
  for(var i= 0; i<4; i++){
  muslims[i]= new Muslim();
 }
}


function draw(){
  background(0);
/*The for-loop checks the "muslims" array
to draw the 4 Muslim objects that should
show on the screen, further more it
specifies the methods that should be inherited
by the the objects. The objects
will all have the proporties of
the constructor );*/
  for (var i= 0;i < muslims.length; i++){
    muslims[i].move();
    muslims[i].display();
    muslims[i].hover();
  }

}

/*This is the constructor function: Muslim.
This is an object constructor that encapsulates
the methods: display, move and hover. Its proporties
are x, y, name,view,diet and gay
*/
function Muslim(){

  let names = ["Muhammed","Mina","Lauren","Bob"]
  let views = ["Conservative","Liberal","Secular"]
  let diet = ["It is forbidden","When in Rome ;)","I eat what is provided"]
  let gays = ["Let me tell you the tragic tale of the sinners of Lot","It is not up to me to judge","Love is love"]

  this.x =random(0,width);
  this.y = random(0,height);
  this.name = random(names);
  this.view = random(views);
  this.diet = random(diet);
  this.gay = random(gays);
//How the objects should manifest on the sketch
  this.display = function(){
    stroke(255);
    noFill();
    ellipse(this.x, this.y,24,24);
  }
//how the objects should move
  this.move= function(){
    this.x= this.x + random(-1, 1);
    this.y = this.y + random(-1,1);
  }
  // an event that triggers an alert
  this.hover = function(){
    let d= dist(mouseX, mouseY, this.x,this.y);
    if (d < 24){
      text("Hi my name is "+(this.name), 10, 30);
      text("My views are  "+(this.view), 10, 50);
      text("What I think of pork? "+(this.diet), 10, 70);
      text("Homosexuality ?"+(this.gay), 10, 90);

    }
  }
}
