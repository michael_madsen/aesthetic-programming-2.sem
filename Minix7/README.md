# Get to know a muslim

 ![ALT](Screenshot_2021-05-19_at_01.13.49.png )

 [My Minix7](https://michael_madsen.gitlab.io/aesthetic-programming-2.sem/Minix7)
 
 ## how does/do your game/game objects work?
 My game consists of ellipses that represents muslims, when you hover over them, they reveal something about themselves.


## Describe how you program the objects and their related attributes, and the methods in your game.
Disclaimer! I looked at the wrong tutorial. I have used a constructor function to create objects. But should have created an object constructor with a class. I do however feel that I demonstrate the intended principles in this minix.

The star of the program is a function called: Muslim. In the function I have some proporties: x,y name, view, diet and gay. All of their values are random. The function also holds 3 methods: display (how the objects should be presented), move (how they should move) and hover( an event that triggers when the courser is over an object). The program shows 4 ellipses/objects that are created by a for-loop. All of the objects have the same proporties and have actively been assigned the same methods. 

## Draw upon the assigned reading, what are the characteristics of object-oriented programming and the wider implications of abstraction?

Objects hold proporties and methods that represents something. The necessary data needed for this representation is gained through `Abstraction`, a both real-life and computational action, which can be seen as the breaking down and extraction and reforming of information. The usefulness of objects lies in their `re-usability`. Through the use of `Object constructional notation` as a `blueprint`you can create multiple objects with the same proporties through fewer lines of code. Relationships can also be formed between objects as they `inherit` characteristics from oneanother. The nature of Objects are to `encapsulate` information. When interacting with an object you will know what methods are hiding between its brackets unless you look into the code or discover them through interaction. The nature and deeper reflexions of `abstraction`lies in the fact that they are representations only and therefor subjective to bias, if we can `model behavior through object`, then we should be aware of this.

### The Obscure Objects of Object Orientation

#### `Abstraction`

"_a computational object is a partial object: the properties and methods that define it are a necessarily selective and creative abstraction of particular capacities from the thing it models, and which specify the ways that it can be interacted with_."- p. 7.

#### `re-usability`
“_But object-oriented programming favours the re-usability of code for computationally abstract kinds of entity and operation – in other words, for entities and operations that are more directly referent to the interfacing of the computer with the world outside_"-p. 8.

#### `Classes & inheritance`

"_A computational object in the object-oriented sense is an instantiation of a class, a programmatically defined construct endowed with specific properties and methods enabling it to accomplish specific tasks. These properties and methods are creative abstractions_."- p. 8.

#### `Encapsulation & Exception handling`

"The term ‘encapsulation’ refers to the way in which object-oriented programming languages facilitate the hiding of both the data that describes the state of the objects that make up a program, and the details of the operations that the object performs." - p. 10.

"exception handling shapes the way in which computational objects respond to anything that exceeds their expectations." - p.11.

#### `modelling behavior with objects`

"_whether computational or not – that may or may not find some ‘representation’ materially in that software. It is, we would suggest, this second aspect of computational objects – modelling behaviour with objects, as opposed to modelling behaviour of objects – that needs to be understood more precisely_"- p. 5

### Aesthetic programming a handbook of software studies

#### `Biased abstraction`

"Therein liespart of the motivation for this chapter, to understand that objects are designed with certain assumptions, biases and worldviews, and to make better object abstractions and ones with a clearer sense of purpose." - p. 164.

#### `Blueprints`

"_To construct objects in OOP it is important to have a blueprint. A class specifies the structure of its objects’ attributes and the possible behaviors/actions of these objects. Class can therefore be understood as a template for, and blueprint of, things._" - p. 155.


## Connect your game project to a wider cultural context, and think of an example to describe how complex details and operations are being “abstracted”?

I made this program as a low-floor commentary on the generalisation/ Biased, non-computational abstraction of muslims as a class in society. Had I used an actually object constructor it would have made more sense since their `class` would be Muslim. Anyways the plain ellipses represent the dark generalisations. The hover effect/ was supposed to be "click", represents the active effort needed to get to know someone. All the objects draw their comments from random arrays to give a sence of how different Muslims are from oneanother, despite the proporties that connects them in the blueprint that is their class. I see the methods that is encapsulated in the object as a way of showing how actions and presentation can seem similar, but still divert in execution. This is what I beleive sets the objects apart.

# References
### Curriculum
- The Obscure Objects of Object Orientation
- Aesthetic programming a handbook of software studies

### Extra
- (was supposed to use this) Object-oriented Programming in 7 minutes | Mosh: https://www.youtube.com/watch?v=pTB0EiLXUC8
- 7.4: The Constructor Function in JavaScript - p5.js Tutorial: https://www.youtube.com/watch?v=F3GeM_KrGjI&t=338s

### Programming
- What are Classes, Objects, and Constructors?: https://www.youtube.com/watch?v=5AWRivBk0Gw
