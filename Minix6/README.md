# Fabulous racist messages and where to find them

![ALT](1200px-Rainbow_flag_and_blue_skies.jpeg )

![ALT](Screenshot_2021-03-22_at_06.25.36.png)

[My Minix6](https://michael_madsen.gitlab.io/aesthetic-programming-2.sem/Minix6/index.html)  
##### _If unable to open in safari, use chrome_

[My Code](https://michael_madsen.gitlab.io/aesthetic-programming-2.sem/Minix6/sketch.js)

### Which MiniX do you plan to rework?
I have chosen to redo my MiniX4 about datacapture, since i never got my original piece to work

### What have you changed and why?
I have made a new program from scratch. My first intended program was meant to use the `Rita Library` to take an input string, convert it to a `Rita-string` and output words that rhymes with the inputted word. My Minix6 genrates random messages when a button is clicked, potentially spamming the screen if the clicking continues.

As Soon adressed in her last lecture we need to focus on the connection, inpact and consequences computation has in our everyday lives and how we through through programming can gain greater understanding of how these things ar possible and the bigger question: Why things are as they are?

 "_to reflect deeply on the pervasiveness of computational culture and its social and political effects - from the language of human-machine languages to abstraction of objects, datafication and recent developments in automated machine intelligence, for example_." - p. 13 


"_take a particular interest in power relations that are under-acknowledged, such as inequalities and injustices related to class, gender and sexuality, as well as race and the enduring legacies of colonialism_." - p. 14 


 "_how power differentials are implicit in code in terms of binary logic, hierarchies, naming of attributes, accessibility, and how wider societal inequalities are further reinforced and perpetuated through non-representational computation_" - p. 14 
 
I have chosen to work with these qoutes since I and many of my POC (Peopl of color) friends have/are experiencing racial discrimination in the gay community, the digitalistion of dating culture has only strengthed this discrimination and through my sketch I would like to let people experience what it feels like to be on the recieving end of some of the messages one can recieve if you do not belong to the anglo-saxon race.

### How would you demonstrate aesthetic programming in your work?
In this sketch I have tried to use programming as a tool to convey a message. My new program is based on the racism gay men experience on online platforms such as Grindr and boyfriend.dk. I have chosen this subject because my sketches has been lacking a critical and cultural connection to software. My new sketch is a faux dating screen, inspired by Grindr. The user will be captured on the screen through their webcam and if they press the orange button they will recieve a message. The message is a racist text-string which is randomly picked from a an array that i have filled with racist messages from Grindr. Should the user continu to click on the orange button, they will eventually create a LGBT-flag inspired screen which is mad up of racist messages, ironically countering the associations of kinship, acceptance and inclussion often promoted in the LGBT community. The use of `createCapture`is an attempt to force the user to embody the experience. To see oneself sorounded by hateful comments, which i try to link to the user through interaction of the button and prompt text. 



![ALT](Screenshot_2021-03-22_at_06.27.08.png ) 

I considered using the 10print model to make the comments appaer after one click, but I simple could not make it look good and was running out of time. I do feel that the manual clicking of the button is a big part of the experience, after seeing one racist message, will you click again? Will you look away? I made it so that each generated message stayed on the screen and accumulated, firstly to spam the user, secondly to emulate the minds of those these comments affect and thirdly to open the dark script of the LGBT that hides behind happy campaigns of Pride, dragshows and unity. The `createCapture`plays an important role in my program because it forces these negative messages onto the user.

- If the user is a gay POC, it reminds them of the discrimination that they face, a problem that should not be ignored.
- If the user is a gay person who has ever been the sender of racist messages, it enforces accountability by removing annonymity. 
- If the user is a non-gay person, I hope that they get a sense of what is going on in LGBT.

### What is the relation between programming and digital culture? + What does it mean by programming as a practice, or even as a method for design?
In the chapter "The Urban Metainterface: Territorial Interfaces" Andersen & Pold talks about the "urban script" and how to acces it. To do thsi they talk about the navigation app "Spoken Streets" by Clara Boj and Diego Diaz. The app gives the user information about different spaces based on location and information taken from the web. The users understanding of a location script broaden through the app.

"_To return to Spoken Streets, this is what it practices. It is—as a book an action—an interface that enriches the reading of the urban, without enforcing functionality and metaphor. It does not enforce specific ways of reading, by controlling and limiting the expressivity of the city, but opens this process_."- p.90

"Spoken streets" is a good example of how digital culture affects the flow of information we recive its potential to affect our peception and thinking, programming can be seen as a tool to understand its influence. Ratto adresses the importance of being critical and reflective through the use of ones tools, not to solve the problem but to reopen and redefine it. 

### Can you draw and link some of the concepts in the text (Aesthetic Programming/Critical Making) and expand on how does your work demonstrate  the perspective of critical-aesthetics?

I do feel that my work adresses Ratto´s point about social reflection.

_"_material engagements with technologies to open up and extend critical social reflectio_n" - p. 18_

My engagement with the code and choice of presentation is a product of my methodology, reflections and expression through computation.

#### References

##### Curriculum
- Andersen, Christian Ulrik, and Soren Bro Pold. The Metainterface : The Art of Platforms, Cities, and Clouds, MIT Press, 2018. ProQuest Ebook
- Aesthetic Programming A Handbook of Software Studies Winnie Soon and Geoff Cox
Published by Open Humanities Press 2020 https://openhumanitiespress.org/
- The Critical Makers Reader: (Un)learning Technology

##### Curriculum extra
- Matt Ratto: Critical Making as an Antidote to Design Thinking: https://www.youtube.com/watch?v=jeBWi_n1Ppg&t=971s

##### Inspiration
- "Spoken Streets" by Clara Boj and Diego Diaz :http://www.lalalab.org/las-calles-habladas/
- New Zealander says passport photo rejection 'not racist':https://www.bbc.com/news/world-asia-38241833
- Preference Or Prejudice? | Queer Britain - Episode 4: https://www.youtube.com/watch?v=U6NtSorRwPs&t=542s
- Grindr Users Talk About Sexual Racism | Kindr Ep. 1:https://www.youtube.com/watch?v=j5_juTW4UTU&t=17s
- This Is What It's Like to Log Into Grindr as a Person of Color :https://www.mic.com/articles/125417/this-is-what-it-s-like-to-log-into-grindr-as-a-person-of-color

##### Syntax
- `setUp & center webcam`:https://editor.p5js.org/denaplesk2/sketches/SJz9Oay6b
- `CreateButton()`:https://p5js.org/reference/#/p5/createButton
- `randomText()`:https://p5js.org/reference/#/p5/random
- `buttonStylling`:https://p5js.org/reference/#/p5.Element/style
- `input & button`:https://p5js.org/examples/dom-input-and-button.html
- `createCapture`:https://www.youtube.com/watch?v=bkGf4fEHKak

