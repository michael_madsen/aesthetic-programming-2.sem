function setup() {

  createCanvas(windowWidth, windowHeight);


}




function draw() {

  background("Grey");
  translate(width / 2, height / 2);

  //fill();
  strokeWeight(10.0);
  strokeJoin(ROUND);



  if (mouseIsPressed) {//if mouse is clicked

    background("pink");//change the background

    circle(50, -50, 30, 20);// draw circles
    circle(150, -50, 30, 20);

    beginShape(); //mouth

    vertex(50, -5); //left point of mouth
    vertex(100, 40); // midlle point of mouth
    vertex(150, -5); //right point of mouth

    endShape(CLOSE);// close gap between the vertecies
    textAlign(CENTER);// Center the text no matter its location

    textSize(50);
    text("Let go for disdain", 100, 100);
  } else { // default state

    arc(50, -50, 40, 40, 0, PI, CHORD);

    arc(150, -50, 40, 40, 0, PI, CHORD);

    beginShape(); //mouth

    vertex(50, -5); //left point of mouth
    vertex(100, -5); // midlle point of mouth
    vertex(150, -5); //right point of mouth

    endShape();
    textAlign(CENTER);
    textSize(50);
    text("click for joy", 100, 100);
  }


}
