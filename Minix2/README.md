## What have I made?

[My Minix2](https://michael_madsen.gitlab.io/aesthetic-programming-2.sem/Minix2/index.html)

[My Code](https://michael_madsen.gitlab.io/aesthetic-programming-2.sem/Minix2/sketch.js)

For my Minix2 I have worked with:

1. Basic geometric shapes
2. Vertices
3. Conditional structure

My program is a basic emoji that can go from showing disdain to joy by clicking, I have added a prompt text to draw atention to this feature.

The structure is a basic draw fuction with a coditional structure that says "if clicked draw 2x circles, change the background, text and create a mouth using three verices-points, if there are no clicks, the default state shoud be a grey background, two archs and a mouth made of three vertices point that align in the same coordinates on the y-axis.

### Syntaxes explained

`background("GREY")`// change the bacground to default grey, no hexcode, no RGB value, just grey

`Translate(width/2,height/2)`// Move the elemnts from the following code to the center, instead of 0,0

`strokeWeight();`// Controls the thickness of shapes and lines

`StrokeJoin(ROUND);`// Rounds out edges

`Circle(50,-50,30,20);`// Creates a circle with the coordinates of 50 x-axis and -50 y-axis, with height of 30 and width of 20.        The coordinates are in based on the centering provided by `Translate`

`beginShape()`// Requires to start `vertices`

`endShape(CLOSE)`// Required to complete and run the `vertices``(CLOSE)`connects the vertices, so they become a complete shape.

`vertex(x,y)`// A vertex is a single Vertices, the x and y paramter controls its location and therefor also the length dawn between the next vertx.

`arc(50,-50,40,0,PI,CHORD);`// Creates the `arc`shape. The first two parameters controls x and y position, following w and h. Next are the degress in radion value that determin the beginning angle and end `CHORD`closes the shape.

`text("",x,y);`// writes text and locates it dependent on the x and y parameter.

`textAlighn(CENTER)`// Centers the text based on defined x and y in `text("",x,y);`

In this minix i have learned how to use vertices and become a little bit more familiar wiith condition structure.

## How would i put my work in relation to the required reading?

Well in the beginning i wanted to make a skull. I wanted to do this because i learned how to stack 3d-shapes via translate, push and pop in my last minix. Another reason was that the problem  with the emojis, seemed to really get bad when they applied the skintone modifiers, so why not cut away the middle-man? Or in this case, skin. I went another route since the text stated that emojis had become over-coded and the point was to convey emotions not humans.

In Our main text-book, there is a passage about an italian artist who tried to draw the [universal] apple. I tried to look it up.

![ALT](apple.jpeg)
_ Enzo Maris [Universal] Apple _

As we can se it is very simple. Riding on that thought i went to:multiculturalkidsblog.com, because i wanted to see how children from around the world would draw a face. Since children usually do not come into this world with advance drawing skills i believed that their minimalistic artistry could help me in this task.

![ALT](diana.jpg) ![ALT](bringing-up-parks.jpg) ![ALT](piripiri-lexicon.jpg)


Children do not have many requirements for a face. A round ball for the head, two round eyes a nose, but never eyebrows. I wanted to do the same, but then I thought about emojis being over-coded, so i chose to only to code a mouth and eyes and use the `arc`shape to emulate eyelids and lowered brow. The choice of using shapes rather than symbols was to deal with cross-platform consistensy. 


