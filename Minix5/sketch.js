
function setup() {
  createCanvas(800,600);
  noLoop();// will create a static sketch
}

function draw() {
  background("#58ECD8");
  
  let step = 70;// The variable that acts as a counter/index. The higher the value, the less patterns will seem to appear since the spacing becomes greater
  for (let x=0;x<width;x=x+step) {//for loop defining x
  	for (let y=0;y<height;y=y+step) {//nested loop defining y
    	strokeWeight(6);
      if(random()>0.7) { // the first condition that creates a "melon wedge" if the value is greater than 0.7. An empty "random()" will choose a value between 0 and 1
        push();
          fill("#DE5171")
          stroke("#2B7B54");  
          arc(x+step, y+step, 50, 50, 0, HALF_PI);//Melon wedge
        pop();
      } else if (random(2)>0.9) {//If the condition is triggered,kiwis and oranges will be created.
          
push();
noStroke();
fill("#F0B253");
ellipse(50+x+step, 50+y+step, 30, 30); // oranges
          ellipseMode(CORNER)
          fill("#3FD864");
ellipse(60+x+step, 40+y+step, 10, 5);//stem leaf  
pop();
noStroke();
ellipseMode(RADIUS);
fill("#A5966E");
ellipse(x+step, y+step, 10, 10); // kiwis
ellipseMode(CENTER);
fill("#B9EC58 ");
ellipse(x+step, y+step, 15, 15);//kiwi inner flesh
      }
      else {// default action
        fill("white");
        stroke("black")
        text('Uhh it´s so juicy', x+step,y);//
      }
    }
  }
}
