# "Tutti Loopy Fruity" a generative piece by Michael Madsen

![ALT](Screenshot_2021-03-14_at_15.04.14.png)

[My Minix5](https://michael_madsen.gitlab.io/aesthetic-programming-2.sem/Minix5/index.html) [My Code](https://michael_madsen.gitlab.io/aesthetic-programming-2.sem/Minix5/sketch.js)

## What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior?
My program has three overall rules:

1. create a certain type of fruit as long as the conditions are met, the order in which this is calculated must be based "randomly" on values between 0 and 1 or 0 and 2.
2. The fruits must be placed relatively to predefined dynamic x and y values.
3. Use the index in a loop as a varible to adjust the spacing between each element.

I have added the `noLoop();`syntax to my program, so that i would get a static sketch. If I remove this syntax, the program willl constantly loop  between every possible outcome within the defined rules. As stated in my rules the placement of the fruie-elements is entirely dependen on the constantly changing x and y values and the index (`step`in my program). The emergent behavior comes from of the relationship between all these parts working together to create new "random" patterns.

![ALT](ezgif.com-gif-maker.gif) 
#### _My program with `noLoop`turned off and `framRate(2);`added_

## What role do rules and processes have in your work?
The structure/process of the program consists of a for-loop that defines x (left-right). In the for-loop there is a nested-loop that defines y(up-down). From the loops the index has been turned into a variable `let step` which can be used by the function. In the loops there is a conditional structure that creates a certain type of fruit dependent on the succes rate in fullfiilling the first condition. Lastly the program is instructed to print the text: "uhh it´s so juicy" if possible.  

## Draw upon the assigned reading, how does this MiniX help you to understand the idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)? Do you have any further thoughts on the theme of this chapter?
This minix has help me in the sense that tecnically it builds upon what we learned about "movement" when we worked with the "throbber". Conceptually it also draws parallels to what we learned about temporality in computation. As humans we have defined a concept of "time" which we try to use through the machine, but the machine in it self has no understanding of time, still we "create" time via computation. In the same sense, we have an understanding of randomness, the machine does not, but just like the "time" we get through computation, randomness via machine is a faux endproduct. Monfort Et al. gives us the root of the word in "10 PRINT CHR$(205.5+RND(1)); : GOTO 10" where it is connected to "_words signifying speed, impulsiveness, and violence._". The moment we write the syntax `random()`the impulsiveness and violence is lost. The program is allowed to act out but only within defined parameters. The randomness in computation is very much controlable, if we look at my program i can actually control the probability of a certain fruit/s appearing. If i want more mellons in the sketch, all i need to do is to increase the probability of the first `condition` in my `if statement` to be true, in this particular case i could lower the condition from 0.7 to 0.2, since the condition is that melons will be created if the paramaters between 0 and 1 are greater 0.2, we know that at least 7 mellons will be drawn. Their placement will be "random", but i could tweak that by adjusting the conditions ind the loop and the index-variable. The number of kiwis and oranges will ofcourse be lowered  since the succes rate of the first condition has been increased and their window to execute are only open in the instances that the first condtion is not met.
![ALT](Screenshot_2021-03-14_at_16.41.17.png)
### References
- https://theibbster.medium.com/a-gentle-introduction-to-coding-by-making-generative-art-c7f0a7b744a6
- https://p5js.org/reference/#/p5/ellipseMode
- https://p5js.org/reference/#/p5/arc
